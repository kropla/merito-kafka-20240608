#### Create docker folder

```shell 
mkdir docker
```

#### Move to created docker folder
```shell
cd docker
```

#### Download docker-compose configuration file with all kafka related configuration
```shell
wget -O kafka-docker-compose.yaml https://raw.githubusercontent.com/conduktor/kafka-stack-docker-compose/master/zk-single-kafka-single.yml
```

#### Run downloaded script - docker should download dependencies and run it in the background
```shell
docker-compose -f ./docker/kafka-docker-compose.yaml up -d
```

#### Check running docker processes
```shell
docker ps
``` 