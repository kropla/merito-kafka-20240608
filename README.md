# Kafka demo project for the lab classes 

1. [Kafka installation](01_KAFKA_INSTALLATION.md)
2. [Gradle configuration for kafka](02_GRADLE_CONFIGURATION.md)
2. [Create and test project](03_TEST_PROJECT.md)
2. [Implement kafka producer](04_KAFKA_PRODUCER.md)
2. [Implement kafka consumer](05_KAFKA_CONSUMER.md)